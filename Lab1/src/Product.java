import static java.sql.Types.NULL;
import java.util.Scanner;

abstract class Product implements ICrudAction {
    private int Id;
    private String Name;
    private float Price;
    private static  int Counter;
    private String Firm;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float price) {
        this.Price = price;
    }

    public static int getCounter() {
        return Counter;
    }

    public static void setCounter(int counter) {
        Counter = counter;
    }

    public String getFirm() {
        return Firm;
    }

    public void setFirm(String firm) {
        this.Firm = firm;
    }

    public static void checkcounter(){
        if (getCounter() == NULL){
            setCounter(0);
        }
    }

    @Override
    public void Update() {
        Scanner in = new Scanner(System.in);
        System.out.println("Input the name");
        String read_name = in.next();
        setName(read_name);

        System.out.println("Input the price");
        float read_price = in.nextInt();
        setPrice(read_price);

        System.out.println("Input the firm");
        read_name = in.next();
        setFirm(read_name);
    }

    @Override
    public void Read() {
        System.out.println(Id+". "+Name+" "+Firm+" "+Price+"$");
    }

    @Override
    public void Delete(){
        this.setId(NULL);
        this.setName("");
        this.setPrice(NULL);
        this.setFirm("");
        Product.setCounter(getCounter()-1);
    }

}

