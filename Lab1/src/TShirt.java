public class TShirt extends Product {

    public TShirt() {
        Product.checkcounter();
        this.setId(Product.getCounter());
        Product.setCounter(Product.getCounter()+1);
    }

    public TShirt(String name, float price, String firm) {
        Product.checkcounter();
        this.setId(Product.getCounter());
        Product.setCounter(Product.getCounter()+1);
        this.setName(name);
        this.setPrice(price);
        this.setFirm(firm);
    }

    @Override
    public void Create(){

    }
}