interface ICrudAction {
    void Create();
    void Read();
    void Update();
    void Delete();
}
