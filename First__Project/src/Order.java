import java.util.Date;

public class Order {
    private ShoppingCard Card;
    private Users User;
    private long createTime;
    private long waitTime;

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public Status getStat() {
        return stat;
    }

    public void setStat(Status stat) {
        this.stat = stat;
    }

    private Status stat;

    public ShoppingCard getCard() {
        return Card;
    }

    public Users getUser() {
        return User;
    }

    public long getCreateTime() {
        return createTime;
    }

    public long getWaitTime() {
        return waitTime;
    }

    public void setCard(ShoppingCard card) {
        Card = card;

    }

    public void setUser(Users user) {
        User = user;
    }

    public void setWaitTime(long waitTime) {
        this.waitTime = waitTime;
    }

    public void SetCreateTime(long time){
        createTime = time;
    }

    public long getTime(){

    }

    public Order(ShoppingCard Card, Users User){
        this.Card = Card;
        this.User = User;
        this.setWaitTime((long)(Math.random())*1000);
    }

}
