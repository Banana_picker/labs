import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args){
        try    {
            System.out.println("Server is running");
            int port  = 3333;
            // создание серверного сокета
            ServerSocket ss = new ServerSocket(port);
            // Ждет клиентов и для каждого создает отдельный поток
            while (true)         {
                Socket s = ss.accept();
                ServerConnectionProcessor p =
                        new ServerConnectionProcessor(s);
                p.start();
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
}
